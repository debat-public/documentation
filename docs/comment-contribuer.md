---
id: comment-contribuer
title: Comment contribuer ?
sidebar_label: Comment contribuer ?
---


Le projet vous intéresse et vous voulez savoir comment contribuer ?

---

### Participez en tant que tisseur de liens
Pour organiser nos actions à l'extérieur de l'école 42, il nous faut établir des liens, disposer de contacts avec des personnes pouvant nous inviter ailleurs.

Nous avons besoin d'invitations de la part d'organisations en tout genre (écoles / organismes publics, entreprises, ...) afin d'y exercer nos actions : permettre à d'autres conférenciers ou organisateurs d'évènements de partager leurs connaissances chez vous.

Si vous avez la possibilité de nous offrir une telle opportunité, merci de nous le faire savoir sur cette page.

---

### Participez financièrement
Vous pouvez faire un don ou adhérer à l'association sur cette page.

Nos adhésions ne seront pas réservées aux étudiants de l'école 42 : toute personne extérieure peut adhérer à l'association et bénéficier des services adhérent au même titre.

---


### Participez autrement
Parlez de nous, envoyez-nous vos avis, vos remarques, vos lettres d'amour, nous acceptons tout :)

---

### Nous n'arriverons pas à nous faire connaître sans votre aide.

Pour chacun des efforts que vous nous consacrerez pour nous aider, y compris celui de lire ces mots... Merci.
