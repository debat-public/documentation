---
id: termes-conditions
title: Termes et conditions
sidebar_label: Termes et conditions
---

Ces conditions d’utilisation présentent les principales caractéristiques de la plateforme de participation en ligne du beta.debatpublic.fr. 

La plateforme est administrée par la Commission nationale du débat public. Les différents processus participatifs ouverts à la participation des citoyens (débats publics et concertations) sont quant à eux modérés par les équipes qui les organisent (commissions particulières et garants).

En accédant à cette plateforme, vous vous engagez à lire et accepter les présentes conditions d’utilisation.

---

## 1/ Domaine d’application des conditions d’utilisation 
Les conditions d’utilisation concernent toutes les personnes physiques et morales qui participent sur cette plateforme. Lors de votre inscription sur beta.debatpublic.fr vous serez amené à accepter les présentes conditions d’utilisation.

La CNDP se réserve le droit de modifier les présentes conditions d’utilisation et s’engage à publier les versions actualisées ici-même. 

---

## 2/ Objectifs de la plateforme beta.debatpublic.fr 
À travers cette plateforme, la CNDP souhaite inciter les citoyens à s’exprimer sur l’orientation à donner à la politique agricole française et à avoir une influence réelle sur la construction de la future PAC.

L’objectif de betas.debatpublic.fr est également de permettre l’inclusion de tous les publics, notamment de ceux qui n’ont pas l’habitude ou la possibilité de se déplacer dans les rencontres publiques. En proposant une plateforme de participation en ligne, la CNDP donne aux participants la possibilité de participer sans leur imposer une contrainte temporelle et physique. Vous pouvez participer d’où vous voulez au moment qui vous convient le mieux.

--- 

## 3/ Présentation des moyens de participation sur la plateforme 
Toute personne physique ou morale inscrite sur la plateforme peut participer aux différents espaces de participation en ligne mis en place par la CNDP. Vous pourrez trouver, dans chaque processus participatif, différents espaces de participation avec des objectifs et des caractéristiques bien précises.

Pour toute question, vous pouvez vous référer à la section d'aide.

---

## 4/ Fonctionnement de la plateforme
#### Participation sur la plateforme

Il est possible de naviguer librement et de façon anonyme à travers la plateforme. En revanche, pour participer à une discussion ou poser une question, il est impératif de s’inscrire sur la plateforme. Pour cela, il faut fournir une adresse électronique personnelle valide. Cette adresse n’est pas publiée sur le site, ni communiquée à un tiers (sauf accord de l’intéressé), son usage est strictement réservé à la CNDP. La CNDP s’autorise toutefois à utiliser cette adresse dans les cas où elle l’estime pertinent, par exemple pour informer son propriétaire de l’ouverture d’un nouveau processus participatif dans sa région et/ou sur une thématique sur laquelle il s’est déjà exprimé.

Les espaces dédiés aux différents processus de participation sont indexés par les moteurs de recherche courants (Google, Yahoo, etc.). Les participants sont donc susceptibles d’apparaître lors d’une recherche de leur nom.

#### Modération 
Chaque message fait l’objet d’une modération. Le rôle du modérateur est de préserver et garantir la qualité des discussions. Il veille en particulier à ne pas publier les messages jugés inappropriés ou qui ne respectent pas la législation en vigueur. Cela concerne notamment les contributions sur le forum, les questions, les avis et tout autre message publié sur la plateforme :

- qui n’est pas en relation avec les questions traitées dans le cadre du processus participatif (hors-sujet) ;
- comportant des attaques, insinuations ou insultes à l’égard d’une organisation ou d’une personne, en particulier si elles sont basées sur la race, les croyances, les origines ethniques ou l’orientation sexuelle ;
- incluant des propos agressifs, méprisants, obscènes ou à caractère pornographique ;
poursuivant des fins commerciales ou publicitaires ;
mentionnant des données personnelles (adresses, numéros de téléphone) ;
contraires aux droits d'auteurs, au respect de la vie privée, au droit à l'image ;
- qui ne sont pas suffisamment argumentées ;
- reproduisant un contenu déjà publié (copier-coller, répétition).
Le modérateur se réserve également le droit de ne pas publier une contribution si cela ne respecte pas les conditions présentées ci-dessus.

#### Comptes d’utilisateurs

Tout internaute désireux de participer sur cette plateforme doit créer un compte d’utilisateur. En tant qu’individu, lors de votre inscription sur la plateforme vous devrez fournir les informations suivantes : nom d’utilisateur (cette information sera rendue publique lors de vos publications, vous pouvez utiliser votre nom, adresse électronique personnelle valide et choisir un mot de passe. Vous devrez également accepter les conditions d’utilisation de la plateforme. À l’issue de la procédure d’inscription, le compte utilisateur est créé et l’internaute reçoit par email une confirmation de sa création qui lui permettra de valider son inscription.  

Si vous représentez une structure (association, chambre consulaire, collectivité, etc.), lors de votre inscription sur la plateforme vous devrez fournir les informations suivantes : nom de l’organisation, nom de la personne responsable, adresse électronique valide, numéro de téléphone et choisir un mot de passe. Vous devrez également accepter les conditions d’utilisation de la plateforme. À l’issue de la procédure d’inscription, le compte utilisateur est créé et l’internaute reçoit par email une confirmation de sa création qui lui permettra de valider son inscription.  

---

## 5/ Protection des données personnelles
Les informations recueillies sur le formulaire d'enregistrement sont enregistrées dans un fichier informatisé par la Commission Nationale du Débat Public pour contacter, en cas de besoin, les citoyens utilisant la plateforme afin de garantir la qualité des débats publics.

Elles sont conservées pendant 6 mois après la clôture du débat concerné et sont destinées au Président ou à la Présidente de chaque Commission Particulière de Débat Public concerné

Conformément à la loi « informatique et libertés », vous pouvez exercer votre droit d'accès aux données vous concernant et les faire rectifier en contactant : Commission Nationale du Débat Public, 244 Bd St Germain - 75016 Paris, contact@debatpublic.fr

La Commission Nationale du Débat Public s’engage à ne pas transmettre vos informations personnelles à des tiers.