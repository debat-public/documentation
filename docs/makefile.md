---
id: makefile
title: Makefile
sidebar_label: Makefile
---

###  Introduction to `Makefile`

> This guide will help you to understand Makefile setup and launch any project of Agora

[Official documentation](https://www.gnu.org/software/make/manual/make.html)