module.exports = {
  someSidebar: {
    Agora: ['introduction', 'design-principles', 'contributing-open-source', 'code-of-conduct'], 
    "Getting Started": ["installation", "configuration", "technos"],
    Guides: ['makefile', 'markdown', 'client-web', 'infra', 'css-debat-public', 'go-backend', 'doc-docusaurus', 'gitlab'],
    "Chartes {Fr}" : ['termes-conditions', 'manifeste', 'accessibilite', 'comment-contribuer'],
  },
};
