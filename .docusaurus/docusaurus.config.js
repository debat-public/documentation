export default {
  "plugins": [],
  "themes": [],
  "customFields": {},
  "themeConfig": {
    "navbar": {
      "title": "Agora",
      "links": [
        {
          "to": "docs/termes-conditions",
          "activeBasePath": "Chartes",
          "label": "Chartes",
          "position": "left"
        },
        {
          "to": "docs/installation",
          "activeBasePath": "Getting Started",
          "label": "Getting Started",
          "position": "left"
        }
      ]
    },
    "footer": {
      "style": "dark",
      "links": [
        {
          "title": "Community",
          "items": [
            {
              "label": "Le débat sur le débat",
              "href": "localhost"
            }
          ]
        },
        {
          "title": "More",
          "items": [
            {
              "label": "Blog",
              "to": "blog"
            },
            {
              "label": "Gitlab",
              "href": "https://gitlab/debat-public"
            }
          ]
        }
      ],
      "copyright": "Copyright © 2020 My Project, Inc. Built with Docusaurus."
    }
  },
  "title": "Agora",
  "tagline": "Free open-source participatory democracy for cities and organizations",
  "url": "https://your-docusaurus-test-site.com",
  "baseUrl": "/",
  "favicon": "img/favicon.ico",
  "organizationName": "debat-public",
  "projectName": "documentation",
  "presets": [
    [
      "@docusaurus/preset-classic",
      {
        "docs": {
          "sidebarPath": "/Users/vguerand/debat_public/documentation/sidebars.js",
          "editUrl": "https://gitlab.com/debat-public/documentation"
        },
        "blog": {
          "showReadingTime": true,
          "editUrl": "https://gitlab.com/debat-public/documentation"
        },
        "theme": {
          "customCss": "/Users/vguerand/debat_public/documentation/src/css/custom.css"
        }
      }
    ]
  ]
};